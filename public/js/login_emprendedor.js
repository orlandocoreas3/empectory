$(document).ready(function () {
    $("#frmLogin").submit(function (e) {
        e.preventDefault();
        ingresar();
    });
});

//CONEXION A FIREBASE
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com/",
    projectId: "empectory-eee4f",
    storageBucket: "",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var database = firebase.database();
var referencia = database.ref("usuarios");

//Url si los datos son correctos para ir a su sesión.
var url = "../vistas/perfil_user.html";

//La funcion para la nueva pestaña web de inicio de sesión.
function abrirEnPestana(){
    
    var a = document.createElement("a");
    a.target = "_self";
    a.href = url;
    a.click();
}

var email;
var password;


//PRUEBA #1
//PARA INGRESAR SOLAMENTE SI EL EMAIL ESTÁ VALIDADO CON SU CORREO.
function ingresar() {

    email = $("#email").val();
    password = $("#password").val();

    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function () {
            emailverificado();
        })
        .catch(function (error) {
            // Handle Errors here.
            // ...
            alertify.error('Email o contraseña incorrectos.');
            document.getElementById("frmLogin").reset();
        });
}
//Para saber si un email esta verificado o no.
function emailverificado() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            // User is signed in.
            var emailVerified = user.emailVerified;
            if (emailVerified == true) {
                abrirEnPestana();
            } else {
                alertify.error('No ha activado su correo, por favor verifiquelo');
                document.getElementById("frmLogin").reset();
            }
        }
    });
}
