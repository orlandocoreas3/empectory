// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//variables que necesito.
var database = firebase.database();
var ref = database.ref("usuarios/");
var otroRef = database.ref("Posts/");
var storage = firebase.storage();
var storageRef = storage.ref();
var empresas = document.querySelector('.mostrar');

mostrarEmpresa();

function mostrarEmpresa() {
    ref.once('value', (data) => {
        var ar = data.val();
        //los usuarios.
        $.each(ar, (indicee, value) => {
            var nombre = value.nombre;
            var email = value.email;
            otroRef.once('value', (datas) => {
                var arr = datas.val();
                $.each(arr, (indice, valor) => {
                    //empresas.
                    var lasempresa = valor;
                    var fo = lasempresa.userPhoto;
                    var email2 = lasempresa.email;
                    var empresa = lasempresa.tittle;
                    if (email == email2) {
                        empresas.innerHTML += `
                        <section>
                                <div class="container" id="${indice}" >
                                    <div class="row">
                                        <div class="col-12 col-md-6 col-lg-4">
                                        <img src="${fo}" id="${indice}" height="300" width="300">
                                        <p id="elNombre"><strong>Nombre: </strong>${nombre}</p>
                                        <p id="parrafo"><strong>Propietario de: </strong>${empresa}</p>
                                        <p id="parrafo"><strong>Email: </strong>${lasempresa.email}</p>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                        </section>
                            `;
                    }
                });
            });
        });
    });
}

/* PERMITE CERRAR SESION CON FIREBASE*/
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    } else {
        //Si no esta iniciada la sesion mandara al login
        location.href = ('../vistas/login_emprendedor.html');
    }
});
//BOTON DE CERRAR SESION.
function cerrarSesion() {
    firebase.auth().signOut().then(function () {
        location.href = ('../vistas/login_emprendedor.html');
        // Sign-out successful.
    }).catch(function (error) {
        alertify.error('No se pudo cerrar sesión');
        // An error happened.
    });
}
//Al hacer clic en la imagen envía el id.
$(document).ready(function () {
    $('body').on('click', 'img', function () {
        var id = $(this).attr('id');
        console.log(id);
        var sep = '?';
        if (id == 'messageAyuda') {
            //Solo esto.
        } else {
            location.href = '../vistas/perfil_solo_emprendedor.html' + sep + id;
        }
    })
})