$(document).ready(function () {
    $("#frmRegistro").submit(function (e) {
        e.preventDefault();

        //TOMA LOS VALORES DE LOS CAMPOS INGRESADOS
        var nombreval = $("#nombre").val();
        var usuarioval = $("#usuario").val();
        var passval = $("#password").val();
        var emailval = $("#email").val();


        //EXPRESIONES REGULARES QUE DAN EL FORMATO A CADA CAMPO
        var exprenombre = /^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ'])?$/;
        var expreusuario = /^[A-Za-z0-9_-]{3,16}$/
        var exprepass = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/

        //VALIDACIONES PARA QUE LOS CAMPOS NO ESTEN VACIOS
        if (nombreval === "") {
            alertify.error('El campo nombre no puede estar vacio');
            return false;
        } else if (usuarioval === "") {
            alertify.error('El campo usuario no puede estar vacio');
            return false;
        } else if (passval === "") {
            alertify.error('El campo contraseña no puede estar vacio');
            return false;
        } else if (emailval === "") {
            alertify.error('El campo email no puede estar vacio');
            return false;
            //VALIDACIONES PARA QUE EL FORMATO DE LOS DATOS SEA CORRECTO
        } else if (!exprenombre.test(nombreval)) {
            alertify.error('Por favor ingrese su nombre completo ejemplo: Roberto Antonio Ayala Vasquez');
            return false;
        } else if (!expreusuario.test(usuarioval)) {
            alertify.error('Por favor ingrese un nombre de usuario valido ejemplo: perez78');
            return false;
        } else if (!exprepass.test(passval)) {
            alertify.error('La contraseña debe contener Mínimo de 8 caracteres 1 mayúsculas, 1 número');
            return false;
        } else {
            registrar();
        }
    });
});


// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com/",
    projectId: "empectory-eee4f",
    storageBucket: "",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//Referencias a la base de datos de FIREBASE.
var database = firebase.database();
var referencia = database.ref("usuarios");

//DIRECCION A REDIRECCIONAR.
var url = "http://localhost/Empectory/public/vistas/login_emprendedor/login_emprendedor.html";

//RUTA PARA ABRIR LA PESTAÑA DE LOGIN.
function abrirEnPestana() {
    var a = document.createElement("a");
    a.target = "_self";
    a.href = url;
    a.click();
}

//VARIABLES PARA USAR EN LAS FUNCIONES.
var nombre;
var usuario;
var password;
var passwordConfirm;
var email;


//PRUEBA DEFINITIVA.
function registrar() {

    //Declaraciones de variables.
    nombre = $("#nombre").val();
    email = $("#email").val();
    password = $("#password").val();
    passwordConfirm = $("#passwordConfirm").val();
    usuario = $("#usuario").val();

    var gratuito = document.getElementById('gratuito').checked;
    var paga = document.getElementById('paga').checked;


    if (password !== passwordConfirm) {
        alertify.error('Las contraseñas no coinciden');
        document.getElementById("frmRegistro").reset();
    } else if (gratuito && paga) {
        alertify.error('Solo debes seleccionar un plan');
    } else if (gratuito == false && paga == false) {
        alertify.error('Selecciona tu plan');
    } else {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function () {
                verificar();
                datosenviar();
                alertify.success('Se ha registrado correctamente');
                document.getElementById("frmRegistro").reset();
            }) //si la funcion no sale bien, mostrara errores.
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode);
                console.log(errorMessage);
                alertify.error('El correo que ingreso ya existe');
                document.getElementById("frmRegistro").reset();
                // ...
            });
    }
}

//enviar datos.
function datosenviar() {

    var gratuito = document.getElementById('gratuito').checked;
    var paga = document.getElementById('paga').checked;
    if (gratuito) {
        enviarPlanesGratis();
    } else if (paga) {
        enviarPlanesPagos();
    }
}

function enviarPlanesGratis() {
    var planGratuito = ('Con plan gratuito');
    var user = firebase.auth().currentUser;
    var uid = user.uid;
    referencia.child(uid).set({
        nombre: nombre,
        email: email,
        usuario: usuario,
        plan: planGratuito
    }).then(function () {
        actualizarPerfil();
    });
}

function enviarPlanesPagos() {

    var planPaga = ('Con plan de paga');
   

    var user = firebase.auth().currentUser;
    var uid = user.uid;
    referencia.child(uid).set({
        nombre: nombre,
        email: email,
        usuario: usuario,
        plan: planPaga
    }).then(function () {
        actualizarPerfil();
    });
}

function actualizarPerfil() {
    var user = firebase.auth().currentUser;
    var avatar = ('https://svgsilh.com/svg/1299805.svg');

    user.updateProfile({
        displayName: nombre,
        photoURL: avatar
    }).then(function () {
        console.log('Se actualizo el displayName');
        // Update successful.
    }).catch(function (error) {
        // An error happened.
    });
}

//fUNCION PARA ENVIAR CORREO.
function verificar() {

    var user = firebase.auth().currentUser;

    user.sendEmailVerification().then(function () {
        // Email sent.
    }).catch(function (error) {
        // An error happened.
    });
}
