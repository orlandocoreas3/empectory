// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


var storage = firebase.storage();
var database = firebase.database();
//inicio de codigo para guardar imagen.
var imagenSeleccionada = document.getElementById("imagen");
imagenSeleccionada.addEventListener('change', function (evt) {
    var user = firebase.auth().currentUser;
    var uid = user.uid;
    var file = evt.target.files[0];
    var storageRef = storage.ref('Fotos de ususarios/' + file.name)
    const referencia = firebase.database().ref('Posts/');
    referencia.once('value', (data) => {
        var ar = data.val();
        $.each(ar, (indice, valor) => {
            var userId = valor.userId;
            var postKey = valor.postKey;
            if (uid == userId) {
                storageRef.put(file).then(function (snapshot) {
                    snapshot.ref.getDownloadURL().then(function (downloadURL) {
                        user.updateProfile({
                            photoURL: downloadURL
                        }).then(function () {
                            alertify.success('Foto Actualizada');
                        }).then(function () {
                            firebase.database().ref('Posts/' + postKey).update({
                                userPhoto: downloadURL
                            });
                        });
                    });
                });
            }
        });
    });
});
//Actualización de nombre y usuario.
var ref = database.ref("usuarios");
var usuario, nombre;
$("#postButton").click(() => {

    nombre = $('#nombre').val();
    usuario = $('#usuario').val();

    var user = firebase.auth().currentUser;
    var uid = user.uid;
    var email = user.email;
    if (nombre == '' && usuario == '') {
        alertify.error('Por favor, rellena los campos requeridos');
    } else {
        ref.child(uid).update({
            nombre: nombre,
            usuario: usuario,
            email: email
        });
        user.updateProfile({
            displayName: nombre
        }).then(function () {
            alertify.success('Información Actualizada');
            document.getElementById('#frmModificarUser');
        });
    }
    // clear your text fields:
    $("input[type=text], textarea").val("");
    return false;
});



//LOGUEADO O NO
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        //Si Inició Sesión.
    } else {
        //Si no esta iniciada la sesion mandara al login
        location.href = ('../vistas/login_emprendedor.html');
    }
});

function cerrarSesion() {
    firebase.auth().signOut().then(function () {
        location.href = ('../vistas/login_emprendedor.html');
        // Sign-out successful.
    }).catch(function (error) {
        alertify.error('No se pudo cerrar sesión');
        // An error happened.
    });
}