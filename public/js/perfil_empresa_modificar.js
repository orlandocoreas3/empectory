$(document).ready(function () {
    $("#frmModificarEmpresa").submit(function (e) {
        e.preventDefault();
        modificarEmpresa();

    });
});

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var storage = firebase.storage();

var database = firebase.database();
var ref = database.ref("Posts");



//LOGUEADO O NO
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        //Si ha iniciado Sesión.
    } else {
        //Si no esta iniciada la sesion mandara al login
        location.href = ('../vistas/login_emprendedor.html');
    }
});

function modificarEmpresa() {
    //declaración de datos a modificar de la empresa.
    var nombre, telefono, direccion, descripcion, facebook, categorias;

    var imagenSeleccionada = document.getElementById("imagen");
    var d = Date.now();
    var file = imagenSeleccionada.files[0];
    var storageRef = storage.ref('post_img/' + file.name + d);


    //captura los datos de entrada.
    nombre = $("#nombre").val();
    telefono = $("#telefono").val();
    direccion = $("#direccion").val();
    descripcion = $("#descripcion").val();
    facebook = $("#facebook").val();
    categorias = $("#categorias").val();
    
    ref.once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnapshot) {
            var user = firebase.auth().currentUser;
            var uid = user.uid;
            var childData = childSnapshot.val();
            var idUsuarioPost = childData.userId;
            var postKey = childData.postKey;
            if (uid == idUsuarioPost) {
                console.log(postKey);
                firebase.database().ref('Posts/' + postKey).update({
                    address: direccion,
                    tittle: nombre,
                    phone: telefono,
                    description: descripcion,
                    facebook: facebook,
                    categorias: categorias
                }).then(function () {
                    alertify.success('Su Empresa ' + nombre + ' ha sido actualizada');
                    storageRef.put(file).then(function (snapshot) {
                        console.log(snapshot);
                        snapshot.ref.getDownloadURL().then(function (downloadURL) {
                            console.log("File available at", downloadURL);
                            firebase.database().ref('Posts/' + postKey).update({
                                picture: downloadURL + d
                            });
                        });
                    });
                    document.getElementById("frmModificarEmpresa").reset();
                });
            }
        });
    });
}

function cerrarSesion() {
    firebase.auth().signOut().then(function () {
        location.href = ('../vistas/login_emprendedor.html');
        // Sign-out successful.
    }).catch(function (error) {
        alertify.error('No se pudo cerrar sesión');
        // An error happened.
    });
}
