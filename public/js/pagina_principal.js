window.addEventListener("load", function () {
  //Cookies
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#03D1Aa"
      },
      "button": {
        "background": "#f1d600"
      }
    },
    "type": "opt-out"
  })
});

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
  authDomain: "empectory-eee4f.firebaseapp.com",
  databaseURL: "https://empectory-eee4f.firebaseio.com",
  projectId: "empectory-eee4f",
  storageBucket: "empectory-eee4f.appspot.com",
  messagingSenderId: "517212836238",
  appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var database = firebase.database();
var ref = database.ref("Subscritos/");
var otroRef = database.ref("Subscritos/");

var name, mail;
//AQUIIII!! MERO!
$("#enviarEmail").click(() => {
  mail = $("#mail").val();
  ref.push({
    mail: mail
  });
});


//FUNCION PARA VOLVER ARRIBA BOTON DE NAVEGACION FLOTANTE
jQuery('document').ready(function ($) {

  var subir = $('.back-to-top');

  subir.click(function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 500);
  });

  subir.hide();

  $(window).scroll(function () {

    if ($(this).scrollTop() > 200) {
      subir.fadeIn();
    } else {
      subir.fadeOut();
    }

  });

});