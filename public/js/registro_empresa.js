$(document).ready(function () {
    $("#frmRegistrar").submit(function (e) {
        e.preventDefault();
        registrar();
    });
});

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

function registrar() {

    //declaración de datos a registrar de la empresa.
    var nombre, telefono, direccion, descripcion, facebook, categorias;
    //captura los datos de entrada.
    nombre = $("#nombre").val();
    telefono = $("#telefono").val();
    direccion = $("#direccion").val();
    descripcion = $("#descripcion").val();
    facebook = $("#facebook").val();
    categorias = $("#categorias").val();
    
    var user = firebase.auth().currentUser;
    var uid = user.uid;
    
    var userPhoto = user.photoURL;
    var email = user.email;
    var imagenSeleccionada = document.getElementById("imagen");
    var storage = firebase.storage();
    var d = Date.now();
    var file = imagenSeleccionada.files[0];
    var storageRef = storage.ref('post_img/' + file.name + d)
    var postKey = firebase.database().ref().child('posts').push().key;

    firebase.database().ref('Posts/' + postKey).set({
        userId: uid,
        email: email,
        address: direccion,
        tittle: nombre,
        phone: telefono,
        description: descripcion,
        facebook: facebook,
        like:0,
        categorias: categorias,
        userPhoto: userPhoto,
        timeStamp: d,
        postKey: postKey
    }).then(function () {
        alertify.success('Su Empresa ' + nombre + ' ha sido registrada');
        storageRef.put(file).then(function (snapshot) {
            console.log(snapshot);
            snapshot.ref.getDownloadURL().then(function (downloadURL) {
                firebase.database().ref('Posts/' + postKey).update({
                    picture: downloadURL + d
                });
            });
        });
        document.getElementById("frmRegistrar").reset();
    });
}


/* PERMITE CERRAR SESION CON FIREBASE*/
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        var database = firebase.database();
        var ref = database.ref("Posts");

        ref.once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var idUsuarioPost = childData.userId;
                var uid = user.uid;
                if (uid == idUsuarioPost) {
                    alertify.alert('Advertencia', 'Su empresa ya ha sido publicada. Mejore su plan para publicar otra empresa.', function(){ location.href = ('../vistas/perfil_user.html'); });
                }
            });
        });
        // ...
    } else {
        //Si no esta iniciada la sesion mandara al login
        location.href = ('../vistas/login_emprendedor.html');
    }
});

//Cierre de Sesión.
function cerrarSesion() {
    firebase.auth().signOut().then(function () {
        location.href = ('../vistas/login_emprendedor.html');
        // Sign-out successful.
    }).catch(function (error) {
        alertify.error('No se pudo cerrar sesión');
        // An error happened.
    });
}
