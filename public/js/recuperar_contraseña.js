$(document).ready(function () {
    $("#frmRecuperacion").submit(function (e) {
        e.preventDefault();
        recuperar();
    });
});

//CONEXION A FIREBASE
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com/",
    projectId: "empectory-eee4f",
    storageBucket: "",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var database = firebase.database();
var referencia = database.ref("usuarios");

//Funcion para enviar correo de recuperacion de contraseña.
function recuperar() {

    //variables importantes para hacer llamados a la base de datos.
    var auth = firebase.auth();
    var emailAddress = $("#email").val();

    auth.sendPasswordResetEmail(emailAddress).then(function () {
        validacion();
        alertify.success('Correo enviado exitosamente, verifique y restaure su contraseña');
        document.getElementById("frmRecuperacion").reset();
        // Email sent.
    }).catch(function (error) {
        // An error happened.
        alertify.error('Este Correo no esta registrado');
    });
}


function validacion() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            // User is signed in.
            var email = ("#email").val();
            var emailexistente = user.email;
            if (email == emailexistente) {
                alertify.success('Correo enviado exitosamente, verifique y restaure su contraseña');
                document.getElementById("frmRecuperacion").reset();
            } else {
                alertify.error('Correo No Existente');
                document.getElementById("frmRecuperacion").reset();
            }
        }
    });
}
