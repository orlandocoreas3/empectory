//AQUI SE MUESTRA EL EMPRENDEDOR DENTRO DE UN INICIO DE SESIÓN.
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//variables del codigo usado mas abajo.
const id = window.location.href.split('?').pop();
var storage = firebase.storage();
var storageRef = storage.ref();
var cont = document.getElementById('contenido');
var cont1 = document.getElementById('comments');
mostrarEmprendedor();

//Trae a los emprendedores individualmente el seleccionado.
function mostrarEmprendedor() {
    const ref = firebase.database().ref('Posts');
    var emprendedor;

    ref.child(id).once('value', (data) => {
        var ar = data.val();
        const fo = ar.picture;
        emprendedor = `<div class="row">
                <div class="col-md-6">
                <img class="ml-3" src="${fo}" height="250" width="250">
                </div>
                <div class="col-md-6">
                <b>Nombre: </b><p>${ar.tittle}</p>
                <b>Dirección: </b><p>${ar.address}</p>
                <b>Descripcion: </b><p>${ar.description}</p>
                <b>Correo Electrónico: </b><p>${ar.email}</p>
                <p><b>Facebook: </b><a href="${ar.facebook}" target="_blank" role="button" aria-pressed="true">${ar.facebook}</a></p>
                <p><em><img src="../img/me_gusta.png" height="25" width="30"> A ${ar.like} personas les gusta ${ar.tittle}<em></p>
                </div>
                </div>
                `;
        cont.innerHTML = emprendedor;
        $(".icon-bar").prepend(`
        <a href="${ar.facebook}" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        `);
        /*<a href="#" class="twitter"><i class="fa fa-twitter"></i></a> 
        <a href="#" class="google"><i class="fa fa-google"></i></a> 
        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="#" class="youtube"><i class="fa fa-youtube"></i></a>*/ 
    });
    
    const empresas = firebase.database().ref('Posts/');
    empresas.once('value', (data) => {
        var ar = data.val();
        $.each(ar, (indice, valor) => {
            let postKey = valor.postKey;
            const dateNews = firebase.database().ref('Publicaciones/' + postKey);
            //Trae sus publicaciones.
            dateNews.on('value', (datos) => {
                var arr = datos.val();
                $.each(arr, (index, value) => {
                    if (postKey == id) {
                        $(".publicaciones").prepend(`<br><div class="row"><div class="col-md-6"><div class="p-3 mb-2 bg-light text-dark ">` + '<p><b>Emprendedor: </b>' + (value.uname) + '<br>' +`<br><img src="${value.picture}"` + ' height="75"/><br>'+ (value.content) + '</p></div></div></div>');
                    }
                })
            });

        });
    });
}

//Trae todos los comentarios correspondientes al id.
const referencia = firebase.database().ref('Comment/' + id);
referencia.on("child_added", (snapshot) => {
    let newPost = snapshot.val();
    var foto = newPost.uimg;

    $(".comments").prepend('<div class="comment">' + '<p id="actualText">' + `<img src="${foto}"` + '?s=100&d=retro" height="20" id="imageStuff"/>' + (newPost.uname) + ': ' + (newPost.content) + '</p></div>');
});


/*`<img src="${foto}"` + '?s=100&d=retro" height="20" id="imageStuff"/>*/