// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
/* PERMITE CERRAR SESION CON FIREBASE*/


//variables que necesito.
var database = firebase.database();
var ref = database.ref("Posts/");
var storage = firebase.storage();
var storageRef = storage.ref();
const formulario = document.querySelector('#formulario');
const boton = document.querySelector('#boton');
const resultado = document.querySelector('#resultado');
var emprendedores = document.querySelector('.mostrar');

//al darle click al boton buscar inicia el filtro.
boton.addEventListener('click', todo)
formulario.addEventListener('keyup', todo)
todo();



function todo() {
    ref.once('value', (data) => {
        emprendedores.innerHTML = '';
        var ar = data.val();
        $.each(ar, (indice, valor) => {
            const fo = valor.picture;
            let nombre = valor.tittle.toLowerCase();
            const texto = formulario.value.toLowerCase();
            if (nombre.indexOf(texto) != -1) {
                emprendedores.innerHTML += `
                <section>
                    <div class="container" id="${indice}" >
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4" >
                            <img src="${fo}" id="${indice}"  height="300" width="300">
                            <p id="parrafo"><strong>Empresa: </strong>${valor.tittle}</p>
                            </div>
                        </div>
                    </div>
                </section>
                `;
            }
        });
    });
}


function categoriass() {
    var combo = document.getElementById('opciones');
    var selected = combo.options[combo.selectedIndex].text;
    var seleccionado = selected.toLowerCase();
    console.log(seleccionado);

    ref.once('value', (data) => {
        emprendedores.innerHTML = '';
        var ar = data.val();
        $.each(ar, (indice, valor) => {
            const fo = valor.picture;
            let categoria = valor.categorias.toLowerCase();
            if (categoria.indexOf(seleccionado) != -1) {
                emprendedores.innerHTML += `
                <section>
                    <div class="container" id="${indice}" >
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4" >
                            <img src="${fo}" id="${indice}"  height="300" width="300">
                            <p id="parrafo"><strong>Empresa: </strong>${valor.tittle}</p>
                            </div>
                        </div>
                    </div>
                </section>
                `;
            }
            if (seleccionado === 'directorio completo') {
                emprendedores.innerHTML += `
                <section>
                    <div class="container" id="${indice}" >
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4" >
                            <img src="${fo}" id="${indice}"  height="300" width="300">
                            <p id="parrafo"><strong>Empresa: </strong>${valor.tittle}</p>
                            </div>
                        </div>
                    </div>
                </section>
                `;
            }
        });
    });
}

$(document).ready(function () {
    $('body').on('click', 'img', function () {
        var id = $(this).attr('id');
        console.log(id);
        var sep = '?';
        if (id == 'messageAyuda') {
            //Solo esto.
        } else {
            location.href = '../vistas/directorio_el_emprendedor.html' + sep + id;
        }
    });
});