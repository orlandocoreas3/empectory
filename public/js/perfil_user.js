// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var storage = firebase.storage();
var database = firebase.database();
var ref = database.ref("usuarios");


$("#irPublicaciones").click(() => {
    location.href = ('../vistas/perfil_publicaciones.html');
});
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        var uid = user.uid;
        const otrareferencia = firebase.database().ref('Posts/');
        //Envío de publicación con imagen.
        $("#publicar").click(() => {
            otrareferencia.once('value', (miData) => {
                let newData = miData.val();
                $.each(newData, (indice, valores) => {
                    var postKey = valores.postKey;
                    var userId = valores.userId;
                    var d = Date.now();
                    var fotoUsuario = user.photoURL;
                    var nombre = user.displayName;
                    let message = $("#publicacion").val();
                    var PublicacionKey = firebase.database().ref().child('PublicacionKey').push().key;
                    var publicaciones = firebase.database().ref('Publicaciones');
                    var publicacionmas = publicaciones.child(postKey);
                    const file = $('#imagen').get(0).files[0];
                    const storageRef = storage.ref('news_img/' + file.name);
                    if (uid == userId) {
                        publicacionmas.child(PublicacionKey).set({
                            uname: nombre,
                            content: message,
                            uid: uid,
                            PublicacionKey: PublicacionKey,
                            uimg: fotoUsuario,
                            timestamp: d
                        }).then(function () {
                            storageRef.put(file).then(function (snapshot) {
                                snapshot.ref.getDownloadURL().then(function (downloadURL) {
                                    console.log(downloadURL);
                                    publicacionmas.child(PublicacionKey).update({
                                        picture: downloadURL + d
                                    }).then(function () {
                                        alertify.success('Se ha publicado con éxito');
                                    });
                                });
                            });
                        });
                        // clear your text fields:
                        $("input[type=text], textarea").val("");
                        document.getElementById("imagen").value = "";
                    } else {
                        alertify.error('No tiene empresa, Publica tu empresa!');
                    }
                });
            });
            return true;
        });
        //Envio del comentario.
        $("#guardar").click(() => {
            otrareferencia.once('value', (miData) => {
                let newData = miData.val();
                $.each(newData, (indice, valores) => {
                    var postKey = valores.postKey;
                    var userId = valores.userId;
                    let message = $("#elNegocio").val();
                    var negocio = firebase.database().ref('Historia');
                    var negociomas = negocio.child(postKey);
                    if (uid == userId) {
                        negociomas.set({
                            history: message
                        }).then(function () {
                            alertify.success('Se ha publicado con éxito');
                        });
                        // clear your text fields:
                        $("input[type=text], textarea").val("");
                    }
                });
            });
        });
        const referencia = database.ref('Historia/');
        //Muestra de la publicación.
        referencia.on("child_added", (snapshot) => {
            var llaves = snapshot.key;
            var contentt = snapshot.val();
            otrareferencia.once('value', (data) => {
                let losDatos = data.val();
                $.each(losDatos, (indice, value) => {
                    var postKey = value.postKey;
                    var userId = value.userId;
                    if (llaves == postKey && userId == uid) {
                        $(".miNegocio").prepend('<div class="p-3 mb-2 bg-light text-dark miContenido">' + '<p class="text-justify">' + (contentt.history) + '</p>' + '</div>' + '<div id="boton_centrado"><input type="button" id="eliminar" class="btn btn-danger" value="Eliminar Publicación"></div></div>');
                        $("#eliminar").click(() => {
                            var refHistoria = database.ref('Historia');
                            refHistoria.child(postKey).remove();
                            location.reload();
                            alertify.success('Se ha borrado su publicación');
                        });
                        var node = document.getElementById("elNegocio");
                        var node2 = document.getElementById("guardar");
                        var node3 = document.getElementById("lapiz");
                        node.parentNode.removeChild(node);
                        node2.parentNode.removeChild(node2);
                        node3.parentNode.removeChild(node3);
                    }
                });
            });
        });
        traerEmprendedor();

        function traerEmprendedor() {
            ref.once('value', (data) => {
                var ar = data.val();
                $.each(ar, (indice, valor) => {
                    if (indice == user.uid) {
                        var fo = user.photoURL;
                        if (fo === null) {
                            var foto =
                                `
                            <div id="foto" alt="imagen">
                            <img src="https://svgsilh.com/svg/1299805.svg" height="300" width="280">
                            </div>
                                `;
                            $(foto).appendTo('.lafoto');
                            // ...  
                        } else {
                            foto =
                                `
                            <div id="foto">
                            <img src="${fo}" height="300" width="280">
                            </div>
                            `;
                            $(foto).appendTo('.lafoto');
                            // ...  
                        }
                        var usuario =
                            `
                        <div id="usuario">
                            <div>
                                <p>
                                <p id="parrafo"><strong>Usuario: </strong>${valor.usuario}</p>
                                <p id="parrafo"><strong>Correo: </strong>${user.email}</p>
                                <p id="parrafo"><strong><i>${valor.plan}</i></strong></p>
                                </p>
                                <br>
                            </div>
                        <div class="Espaciador">&nbsp;</div>
                        `;
                        $(usuario).appendTo('.columnPerfil');

                        var contenido =
                            `
                            <div>
                            <p id="parrafo"><strong>¡Bienvenido </strong>${user.displayName }!</p>
                            <img src="../img/ban_perfil.png" alt="" height="300" width="600">
            
                            </div>
                            </div>
                            `;
                        $(contenido).appendTo('.contenido');
                    }
                });
            });
        }


        //Traer Publicaciones.
        otrareferencia.once('value', (miData) => {
            let newData = miData.val();
            $.each(newData, (indice, valores) => {
                var postKey = valores.postKey;
                const publicacionRef = database.ref('Publicaciones/' + postKey);
                publicacionRef.on("child_added", (snapshot) => {
                    let newPost = snapshot.val();
                    let llave = snapshot.key;
                    let userId = newPost.uid;
                    if(userId == uid){
                        $(".publicaciones").prepend(`<br><br><div class="row"><div class="col-md-6"><div class="p-3 mb-2 bg-light text-dark ">` + '<p><b>Emprendedor: </b>' + (newPost.uname) + '<br>' + `<br><img src="${newPost.picture}"` + ' height="75"/><br>' + (newPost.content) + '<br><br><input type="button" id="eliminar2" class="btn btn-danger" value="Eliminar"></p></div></div></div>');
                        $("#eliminar2").click(() => {
                            var refPublicaciones = firebase.database().ref('Publicaciones');
                            var negociomas = refPublicaciones.child(postKey);
                            negociomas.child(llave).remove();
                            location.reload();
                        });
                    }
                });
            });
        });

        //Eliminación de cuenta firebase
        $("#cerrarCuenta").click(() => {
            let userRef = database.ref('usuarios/' + uid);
            userRef.once('value', (datosUsers) => {
                let myKey = datosUsers.key;
                if (myKey == uid) {
                    ref.child(myKey).remove();
                    eliminarAuthUser();
                } else {
                    alertify.error('Por favor, Cierra y vuelve a iniciar sesión para eliminar tu cuenta');
                }
                otrareferencia.once('value', (miData) => {
                    let newData = miData.val();
                    $.each(newData, (indice, valores) => {
                        let postKey = valores.postKey;
                        let elId = valores.userId;
                        if(uid == elId){
                            otrareferencia.child(postKey).remove();
                        }
                    });
                });
            });
        });

        function eliminarAuthUser() {
            user.delete().then(function () {
                alertify.success('TE HAS DADO DE BAJA DE EMPECTORY CON ÉXITO. SE HAN ELIMINADO TUS DATOS PERSONALES Y TODA TU ACTIVIDAD.');
                // User deleted.
            }).catch(function (error) {
                console.log(user);
                alertify.error('Por favor, Cierra y vuelve a iniciar sesión para eliminar tu cuenta');
                // An error happened.
            });
        }

        // User is signed in.
    } else {
        //Si no esta iniciada la sesion mandara al login
        location.href = ('../vistas/login_emprendedor.html');
    }
});

function cerrarSesion() {
    firebase.auth().signOut().then(function () {
        location.href = ('../vistas/login_emprendedor.html');
        // Sign-out successful.
    }).catch(function (error) {
        alertify.error('No se pudo cerrar sesión');
        //Ha ocurrido un error.
    });
}