//AQUI SE MUESTRA EL EMPRENDEDOR DENTRO DE UN INICIO DE SESIÓN.
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB4MqvDxdnRWTOoF_a03j0o3twa9L2J0Ic",
    authDomain: "empectory-eee4f.firebaseapp.com",
    databaseURL: "https://empectory-eee4f.firebaseio.com",
    projectId: "empectory-eee4f",
    storageBucket: "empectory-eee4f.appspot.com",
    messagingSenderId: "517212836238",
    appId: "1:517212836238:web:f597f272a8701536"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//variables del codigo usado mas abajo.
const id = window.location.href.split('?').pop();
var storage = firebase.storage();
var storageRef = storage.ref();
var cont = document.getElementById('contenido');
var cont1 = document.querySelector('.comments');
mostrarEmprendedor();

//Trae a los emprendedores individualmente el seleccionado.
function mostrarEmprendedor() {
    const ref = firebase.database().ref('Posts/');
    var otroRef = firebase.database().ref("usuarios/");
    var emprendedor;
    var emprendedor2;
    ref.child(id).once('value', (data) => {
        //Trae a las empresas con su respectivo id.
        var ar = data.val();
        const fo = ar.userPhoto;
        const uid = ar.userId;
        const email = ar.email;
        var masRef = firebase.database().ref('Negocio/');
        var maasRef = masRef.child(uid);
        otroRef.once('value', (datas) => {
            //Trae a los usuario y los valida con su respectiva empresa.
            var arr = datas.val();
            $.each(arr, (index, valor) => {
                var email2 = valor.email;
                var usuario = valor.usuario;
                if (email == email2) {
                    emprendedor = `<div class="row">
                    <div class="col-md-6">
                    <img class="ml-3" src="${fo}" height="250" width="250">
                    </div>
                    <div class="col-md-6">
                    <b>Nombre: </b><p>${usuario}</p>
                    <b>Propietario de la empresa: </b>
                    <br>
                    <img src="${ar.picture}"?s=100&d=retro" height="100" id="imageStuff"/>
                    <br>
                    <p>${ar.tittle}</p>
                    <b>Correo Electrónico: </b><p>${email2}</p>
                    <p><b>Facebook: </b><a href="${ar.facebook}" target="_blank" role="button" aria-pressed="true">${ar.facebook}</a></p>
                    </div>
                    </div>
                    `;
                    cont.innerHTML = emprendedor;
                    maasRef.once('value', (datass) => {
                        //Trae a su como empezó su negocio con su respectivo id.
                        var arrr = datass.val();
                        $.each(arrr, (indice, valor2) => {
                            var negociouid = valor2.uid;
                            if(uid == negociouid){
                                emprendedor2 =
                                `
                                <div class="miLabel"><p><strong>¿Como inició mi negocio?</strong></p></div>
                                <div class="comments">
                                <p>${valor2.content}</p>
                                </div>
                                <br>
                                <br>
                                `;
                                cont1.innerHTML = emprendedor2;
                            } 
                        });
                    });
                }
            });
        });
    });
}

//textos.
const makeClean = (text) => {
    return text.toString().toLowerCase().trim()
        .replace(/&/g, '-and-')
        .replace(/[\s\W-]+/g, '-')
        .replace(/[^a-zA-Z0-9-_]+/g, '');
}

const referencia = firebase.database().ref('Negocio/' + id);

referencia.on("child_added", (snapshot) => {
    let newPost = snapshot.val();
    var foto = newPost.uimg;

    $(".comments").prepend('<div class="comment">' + '<p id="actualText">' + `<img src="${foto}"` + '?s=100&d=retro" height="20" id="imageStuff"/>' + (newPost.uname) + ': ' + (newPost.content) + '</p></div>');
});
/* PERMITE CERRAR SESION CON FIREBASE*/
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    } else {
        //Si no esta iniciada la sesion mandara al login
        location.href = ('../vistas/login_emprendedor.html');
    }
});
//BOTON DE CERRAR SESION.
function cerrarSesion() {
    firebase.auth().signOut().then(function () {
        location.href = ('../vistas/login_emprendedor.html');
        // Sign-out successful.
    }).catch(function (error) {
        alertify.error('No se pudo cerrar sesión');
        // An error happened.
    });
}